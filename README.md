# Emzi's personal website
This is the source code of my personal website, available at https://emzi0767.com/. It's an ASP.NET Core 2.2 MVC application, not really designed for self-hosting.

To be honest I am not sure why I am sharing this. It's neither a shining example of How to ASP.NET Core, nor is it really anything I expect anyone to use or contribute to.

## Complaints
If you find a bug or have a suggestion for an improvement, feel free to [open an issue](https://bitbucket.org/Emzi0767/emzi0767.com/issues?status=new&status=open). Make sure to describe the problem throughly.

## Requirements
If you're actually hellbent on running this, you will need the following:

* **Alpine Linux v3.9** - The entire project is set up to run on Alpine Linux 3.9.
* **Docker** - It's strongly recommended you run the application inside Docker and expose the TCP port it will use to communicate with outside. This also lets you restrict the resources available to the 
  application.
* **nginx** - To act as reverse proxy. It should be placed in front of Docker, and route requests to the appropriate container.
* **Let's encrypt certbot** - In current year, you *must* use SSL for your website. Cerbot will automate certificate management and renewal.
* **.NET Core 2.2 runtime** - The application is an ASP.NET Core 2.2 app, which itself runs on top of .NET Core 2.2 runtime. You need to install that inside your docker container.
* **CloudFlare** - While this setup should be able to handle a sudden influx of requests, however it's still recommended you put something to cache and handle traffic influx in front of your setup.
* **Decent hardware** - It's a good idea to reserve at least 2 CPU cores and 1GB RAM for the application. You also need to count in the requirements for nginx, Postgres, Docker, and the OS itself.

### Setting up the environment
This is nothing overly complicated, but I'll leave the specifics to figure out yourself.

1. Create a docker container for the application. The container should have:
   * Alpine Linux
   * ASP.NET Core 2.2 runtime
   * The application
2. Put your application in the container.
3. Create an `appsettings.json` file with proper config values.
4. Run the application in Docker.
5. Configure nginx:
   * Pass all `http://` requests to `https://` (forcing SSL).
   * Pass `https://*/.well-known` requests to your `.well-known` directory inside your web root.
   * Pass all other `https://` requests to `https://your-docker-container`.
   * Rewrite responses as necessary.
8. Configure Let's encrypt certbot.
9. Configure CloudFlare.

That should be about it. The application should now be running.