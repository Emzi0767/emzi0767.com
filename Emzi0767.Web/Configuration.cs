﻿using System.IO;
using System.Text;

namespace Emzi0767.Web.Configuration
{
    public class ApplicationConfig
    {
        public KeyManagerConfig KeyManager { get; set; }

        public SslConfig Ssl { get; set; }
        
        public ContentConfig Content { get; set; }
    }

    public class KeyManagerConfig
    {
        public string Store { get; set; }
    }

    public class SslConfig
    {
        public CertificateConfig Certificate { get; set; }
    }

    public class CertificateConfig
    {
        public string File { get; set; }

        public string EncryptedPassword { get; set; }

        public string LoadEncryptedPassword()
        {
            var file = new FileInfo(this.EncryptedPassword);
            using (var fs = file.OpenRead())
            using (var sr = new StreamReader(fs, new UTF8Encoding(false)))
                return sr.ReadToEnd().Trim();
        }
    }

    public class ContentConfig
    {
        public CryptocurrencyAddressConfig Cryptocurrency { get; set; }
    }

    public class CryptocurrencyAddressConfig
    {
        public string Ethereum { get; set; }
        
        public string Litecoin { get; set; }
        
        public string Bitcoin { get; set; }
    }
}
