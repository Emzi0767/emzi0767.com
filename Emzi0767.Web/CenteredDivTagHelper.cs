﻿using System.Linq;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace Emzi0767.Web
{
    [HtmlTargetElement("div", Attributes = "emzi-center-content")]
    public class CenteredDivTagHelper : TagHelper
    {
        [HtmlAttributeName("emzi-center-content")]
        public bool Center { get; set; }

        [ViewContext]
        public ViewContext ViewContext { get; set; }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            if (this.Center)
            {
                var @class = output.Attributes.FirstOrDefault(xa => xa.Name == "class");
                if (@class == null)
                {
                    output.Attributes.Add("class", "content-align-center");
                }
                else
                {
                    output.Attributes.Remove(@class);
                    output.Attributes.Add("class", string.Concat(@class.Value, " content-align-center"));
                }
            }
        }
    }
}
