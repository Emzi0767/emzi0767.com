﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.TagHelpers;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace Emzi0767.Web
{
    [HtmlTargetElement("li", Attributes = "emzi-bootstrap-link")]
    public sealed class NavTagHelper : AnchorTagHelper
    {
        public static string[] DiscordActions { get; } = new[] { "Index", "CompanionCube", "MusicTurret", "Ada", "Pam", "Asm" };
        public static string[] ProjectsActions { get; } = new[] { "Index", "ClydeDotNet" };

        [HtmlAttributeName("emzi-bootstrap-link")]
        public string ControllerActionCombo { get; set; }

        [HtmlAttributeName("emzi-active-actions")]
        public string[] ActiveActions { get; set; }

        public NavTagHelper(IHtmlGenerator generator) 
            : base(generator)
        { }

        public override async Task ProcessAsync(TagHelperContext context, TagHelperOutput output)
        {
            var req = this.ViewContext.HttpContext.Request;

            var cc = await output.GetChildContentAsync();
            var cnt = cc.GetContent();

            var a = this.Generator.GenerateActionLink(this.ViewContext, cnt, this.Action, this.Controller, null, null, null, null, null);
            var href = a.Attributes["href"];
            output.Content.SetHtmlContent(string.Concat("<a href=\"", href, "\">", cnt, "</a>"));

            var atc = this.Controller.ToLower();
            var acc = (this.ViewContext.RouteData.Values["controller"] as string).ToLower();

            var ata = this.Action.ToLower();
            var aca = (this.ViewContext.RouteData.Values["action"] as string).ToLower();

            var aaa = false;
            if (this.ActiveActions != null && this.ActiveActions.Any())
                aaa = this.ActiveActions.Any(xs => xs.ToLower() == aca);

            if (acc == atc && (aca == ata || aaa))
            {
                var @class = output.Attributes.FirstOrDefault(xa => xa.Name == "class");
                if (@class == null)
                {
                    output.Attributes.Add("class", "active");
                }
                else
                {
                    output.Attributes.Remove(@class);
                    output.Attributes.Add("class", string.Concat(@class.Value, " active"));
                }
            }
        }
    }
}
