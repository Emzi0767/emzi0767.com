﻿using System;
using System.Text;
using Microsoft.AspNetCore.Mvc;

namespace Emzi0767.Web
{
    public static class RazorHelpers
    {
        private static UTF8Encoding UTF8 { get; } = new UTF8Encoding(false);

        public static string FullContentUrl(this IUrlHelper urlhelper, string vpath)
        {
            var req = urlhelper.ActionContext.HttpContext.Request;

            var url = string.Concat(req.Scheme, "://", req.Host.ToUriComponent(), urlhelper.Content(vpath));
            return url;
        }

        public static string ToBase64<T>(this T value)
        {
            var bts = value as byte[];
            if (bts == null)
            {
                var str = value as string;
                if (str == null)
                    str = value.ToString();

                bts = UTF8.GetBytes(str);
            }

            return Convert.ToBase64String(bts).Replace("/", "_").Replace("+", "-").Replace("=", "'");
        }

        public static byte[] BytesFromBase64(this string value)
        {
            var bts = Convert.FromBase64String(value.Replace("_", "/").Replace("-", "+").Replace("'", "="));
            return bts;
        }

        public static string StringFromBase64(this string value)
        {
            var bts = value.BytesFromBase64();
            var str = UTF8.GetString(bts);
            return str;
        }

        public static int CalculateYearsTo(this DateTimeOffset dto, DateTimeOffset until)
        {
            var zerotime = new DateTimeOffset(1, 1, 1, 0, 0, 0, TimeSpan.Zero);
            var a = dto.ToUniversalTime();
            var b = until.ToUniversalTime();

            var ts = b - a;
            var yrs = (zerotime + ts).Year - 1;

            return yrs;
        }
    }
}
