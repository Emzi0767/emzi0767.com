﻿using System;
using System.Globalization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;

namespace Emzi0767.Web
{
    /// <summary>
    /// Constrains a route parameter to not contain a specified string.
    /// </summary>
    public class NotStringRouteConstraint : IRouteConstraint
    {
        private string _value;

        /// <summary>
        /// Initializes a new instance of the <see cref="StringRouteConstraint"/> class.
        /// </summary>
        /// <param name="value">The constraint value to match.</param>
        public NotStringRouteConstraint(string value)
        {
            if (value == null)
                throw new ArgumentNullException(nameof(value));

            _value = value;
        }

        /// <inheritdoc />
        public bool Match(HttpContext httpContext, IRouter route, string routeKey, RouteValueDictionary values, RouteDirection routeDirection)
        {
            if (httpContext == null)
                throw new ArgumentNullException(nameof(httpContext));

            if (route == null)
                throw new ArgumentNullException(nameof(route));

            if (routeKey == null)
                throw new ArgumentNullException(nameof(routeKey));

            if (values == null)
                throw new ArgumentNullException(nameof(values));

            if (values.TryGetValue(routeKey, out var routeValue) && routeValue != null)
            {
                var parameterValueString = Convert.ToString(routeValue, CultureInfo.InvariantCulture);
                return !parameterValueString.Equals(_value, StringComparison.OrdinalIgnoreCase);
            }

            return false;
        }
    }
}
