﻿using Emzi0767.Web.Configuration;
using Emzi0767.Web.Crypto;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Emzi0767.Web
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc()/*.SetCompatibilityVersion(Microsoft.AspNetCore.Mvc.CompatibilityVersion.Version_2_2)*/;

            services.Configure<ApplicationConfig>(opts => this.Configuration.GetSection("Application").Bind(opts))
                .Configure<ContentConfig>(opts => this.Configuration.GetSection("Application:Content").Bind(opts))
                .Configure<SslConfig>(opts => this.Configuration.GetSection("Application:SSL").Bind(opts))
                .Configure<KeyManagerConfig>(opts => this.Configuration.GetSection("Application:KeyManager").Bind(opts))
                .AddSingleton<KeyManager>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            app.UseStaticFiles();
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller}/{action}",
                    defaults: new { controller = "Home", action = "Index" }/*,
                    constraints: new { controller = new NotStringRouteConstraint("Utility") }*/);

                routes.MapRoute(
                    name: "api_qr",
                    template: "api/util/qr/{id}/{size?}",
                    defaults: new { controller = "Utility", id = "undefined" });
            });
        }
    }
}
