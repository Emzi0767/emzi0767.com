﻿using System.Net;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using Emzi0767.Web.Configuration;
using Emzi0767.Web.Crypto;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace Emzi0767.Web
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args)
        {
            return WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .UseKestrel((ctx, ko) =>
                {
                    ko.Listen(IPAddress.Any, 5000, lo =>
                    {
                        var km = lo.ApplicationServices.GetRequiredService<KeyManager>();
                        var ssl = lo.ApplicationServices.GetRequiredService<IOptions<SslConfig>>().Value;
                        var cert = ssl.Certificate;

                        var crt = cert.File;
                        // Load from file instead
                        //var cpb = cert.EncryptedPassword.BytesFromBase64();
                        var cpb = cert.LoadEncryptedPassword().BytesFromBase64();

                        km.Decrypt(cpb, out var cpw, "pfxkey");

                        lo.Protocols = HttpProtocols.Http1AndHttp2;
                        lo.UseHttps(new X509Certificate2(crt, cpw), httpso =>
                        {
                            httpso.SslProtocols = SslProtocols.Tls12;
                        });
                    });
                });
        }
    }
}
