﻿using System;
using Emzi0767.Web.Configuration;
using Emzi0767.Web.Crypto;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Options;

namespace Emzi0767.Web.Controllers
{
    public abstract class EmziControllerBase : Controller
    {
        protected ContentConfig ContentConfiguration { get; }
        protected KeyManager KeyManager { get; }

        public EmziControllerBase(IOptions<ContentConfig> contentCfg, KeyManager keyManager)
        {
            this.ContentConfiguration = contentCfg.Value;
            this.KeyManager = keyManager;
        }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            base.OnActionExecuting(context);

            this.ViewData["crypto:eth"] = this.ContentConfiguration.Cryptocurrency.Ethereum;
            this.ViewData["crypto:ltc"] = this.ContentConfiguration.Cryptocurrency.Litecoin;
            this.ViewData["crypto:btc"] = this.ContentConfiguration.Cryptocurrency.Bitcoin;

            var req = this.HttpContext.Request;

            var urlb = new UriBuilder
            {
                Scheme = req.Scheme,
                Host = req.Host.Host,
                Path = req.Path,
                Query = req.QueryString.Value
            };
            if (req.Host.Port != null && req.Host.Port.HasValue)
                urlb.Port = req.Host.Port.Value;

            this.ViewData["context:url"] = urlb.Uri.ToString();
        }
    }
}
