﻿using Emzi0767.Web.Configuration;
using Emzi0767.Web.Crypto;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace Emzi0767.Web.Controllers
{
    public class ProjectsController : EmziControllerBase
    {
        public ProjectsController(IOptions<ContentConfig> contentCfg, KeyManager keyManager) : base(contentCfg, keyManager) { }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult ClydeDotNet()
        {
            return View();
        }
    }
}
