﻿using System.Diagnostics;
using Emzi0767.Web.Configuration;
using Emzi0767.Web.Crypto;
using Emzi0767.Web.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace Emzi0767.Web.Controllers
{
    public class HomeController : EmziControllerBase
    {
        public HomeController(IOptions<ContentConfig> contentCfg, KeyManager keyManager) : base(contentCfg, keyManager) { }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult About()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
