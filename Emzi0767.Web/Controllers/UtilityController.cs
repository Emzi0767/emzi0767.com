﻿using System.Text;
using Emzi0767.Web.Configuration;
using Emzi0767.Web.Crypto;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using ZXing;
using ZXing.QrCode;
using ZXing.QrCode.Internal;

namespace Emzi0767.Web.Controllers
{
    [Route("api/util/[action]")]
    public class UtilityController : EmziControllerBase
    {
        private static UTF8Encoding UTF8 { get; } = new UTF8Encoding(false);

        public UtilityController(IOptions<ContentConfig> contentCfg, KeyManager keyManager) : base(contentCfg, keyManager) { }

        [HttpGet]
        public IActionResult Index()
        {
            return new BadRequestResult();
        }
        
        [ActionName("qr")]
        [HttpGet("{id}/{size:int=256}")]
        public IActionResult QRCode(string id, int size)
        {
            var zxsvg = new BarcodeWriterSvg
            {
                Format = BarcodeFormat.QR_CODE,
                Options = new QrCodeEncodingOptions
                {
                    ErrorCorrection = ErrorCorrectionLevel.L,
                    Width = size,
                    Height = size,
                    Margin = 1
                }  
            };
            var svg = zxsvg.Write(id.StringFromBase64()).ToString()
                .Replace("255,255,255", "17,17,17")
                .Replace("0,0,0", "221,221,221");

            return this.Content(svg, "image/svg+xml", UTF8);
        }

        [ActionName("test")]
        [HttpGet]
        public IActionResult TestOK()
        {
            return this.Content("Routing successful", "text/plain", UTF8);
        }
    }
}
