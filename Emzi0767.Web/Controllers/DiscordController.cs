﻿using Emzi0767.Web.Configuration;
using Emzi0767.Web.Crypto;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace Emzi0767.Web.Controllers
{
    [Route("[controller]/[action]")]
    public class DiscordController : EmziControllerBase
    {
        public DiscordController(IOptions<ContentConfig> contentCfg, KeyManager keyManager) : base(contentCfg, keyManager) { }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet("{id?}")]
        public IActionResult CompanionCube(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
                return View();
            else if (id.ToLower() == "doc")
                return View("CompanionCubeDoc");
            else if (id.ToLower() == "invite")
                return new RedirectResult("https://discordapp.com/oauth2/authorize?client_id=276042646693216258&scope=bot&permissions=0", true);
            else if (id.ToLower() == "source")
                return new RedirectResult("https://github.com/Emzi0767/Discord-Companion-Cube-Bot", true);
            return View();
        }

        [HttpGet("{id?}")]
        public IActionResult MusicTurret(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
                return View();
            else if (id.ToLower() == "doc")
                return View("MusicTurretDoc");
            else if (id.ToLower() == "invite")
                return new RedirectResult("https://discordapp.com/oauth2/authorize?client_id=469727979614765066&scope=bot&permissions=0", true);
            else if (id.ToLower() == "source")
                return new RedirectResult("https://github.com/Emzi0767/Discord-Music-Turret-Bot", true);
            return View();
        }

        [HttpGet("{id?}")]
        public IActionResult Ada(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
                return View();
            else if (id.ToLower() == "doc")
                return View("AdaDoc");
            //else if (id.ToLower() == "invite")
            //    return new RedirectResult("https://discordapp.com/oauth2/authorize?client_id=207900447044927488&scope=bot&permissions=2146958591", true);
            else if (id.ToLower() == "source")
                return new RedirectResult("https://github.com/Emzi0767/Discord-ADA-Bot", true);
            return View();
        }

        [HttpGet("{id?}")]
        public IActionResult Pam(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
                return View();
            else if (id.ToLower() == "doc")
                return View("PamDoc");
            else if (id.ToLower() == "invite")
                return new RedirectResult("https://discordapp.com/oauth2/authorize?client_id=262603614890098689&scope=bot&permissions=2146958591", true);
            else if (id.ToLower() == "source")
                return new RedirectResult("https://github.com/Emzi0767/Discord-PAM-Bot", true);
            return View();
        }

        [HttpGet("{id?}")]
        public IActionResult Asm(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
                return View();
            else if (id.ToLower() == "doc")
                return View("AsmDoc");
            else if (id.ToLower() == "invite")
                return new RedirectResult("https://discordapp.com/oauth2/authorize?client_id=283200903937261569&scope=bot&permissions=0", true);
            else if (id.ToLower() == "source")
                return new RedirectResult("https://github.com/Emzi0767/Discord-ASM-Bot", true);
            return View();
        }
    }
}
