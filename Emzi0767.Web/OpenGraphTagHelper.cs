﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace Emzi0767.Web
{
    [HtmlTargetElement("meta", Attributes = "opengraph-element,opengraph-value,opengraph-default-value")]
    public sealed class OpenGraphTagHelper : TagHelper
    {
        [HtmlAttributeName("opengraph-element")]
        public OpenGraphElement OgElement { get; set; }

        [HtmlAttributeName("opengraph-value")]
        public string OgValue { get; set; }

        [HtmlAttributeName("opengraph-default-value")]
        public string OgDefaultValue { get; set; }

        [HtmlAttributeName("opengraph-value-suffix")]
        public string OgValueSuffix { get; set; }

        [ViewContext]
        public ViewContext ViewContext { get; set; }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            var name = "og:this_is_a_serious_error";
            switch (this.OgElement)
            {
                case OpenGraphElement.Title:
                    name = "og:title";
                    break;

                case OpenGraphElement.Type:
                    name = "og:type";
                    break;

                case OpenGraphElement.Image:
                    name = "og:image";
                    break;

                case OpenGraphElement.Url:
                    name = "og:url";
                    break;

                case OpenGraphElement.Description:
                    name = "og:description";
                    break;
            }
            output.Attributes.Add("name", name);

            var val = string.IsNullOrWhiteSpace(this.OgValue) ? this.OgDefaultValue : this.OgValue;
            if (!string.IsNullOrWhiteSpace(this.OgValueSuffix))
                val = string.IsNullOrWhiteSpace(val) ? this.OgValueSuffix : string.Concat(val, " - ", this.OgValueSuffix);
            output.Attributes.Add("content", val);
        }
    }

    public enum OpenGraphElement : int
    {
        Title, Type, Url, Image, Description
    }
}
