﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace Emzi0767.Web
{
    [HtmlTargetElement("meta", Attributes = "emzi-description,emzi-default-description")]
    public sealed class MetaTagHelper : TagHelper
    {
        [HtmlAttributeName("emzi-description")]
        public string Description { get; set; }

        [HtmlAttributeName("emzi-default-description")]
        public string DefaultDescription { get; set; }

        [ViewContext]
        public ViewContext ViewContext { get; set; }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            var desc = string.IsNullOrWhiteSpace(this.Description) ? this.DefaultDescription : this.Description;
            output.Attributes.Add("content", desc);
        }
    }
}
